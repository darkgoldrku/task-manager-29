package ru.t1.bugakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.enumerated.Status;
import ru.t1.bugakov.tm.model.Task;

import java.util.List;

public interface ITaskService extends IAbstractUserOwnedService<Task> {

    @NotNull
    Task create(@NotNull final String userId, @Nullable final String name, @Nullable final String description);

    @NotNull
    List<Task> findAllByProjectId(@NotNull final String userId, @Nullable final String projectId);

    @NotNull
    Task updateById(@NotNull final String userId, @Nullable final String id, @Nullable final String name, @NotNull final String description);

    @NotNull
    Task updateByIndex(@NotNull final String userId, @Nullable final Integer index, @Nullable final String name, @NotNull final String description);

    @NotNull
    Task changeTaskStatusById(@NotNull final String userId, @Nullable final String id, @NotNull final Status status);

    @NotNull
    Task changeTaskStatusByIndex(@NotNull final String userId, @Nullable final Integer index, @NotNull final Status status);

}
