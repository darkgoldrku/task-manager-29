package ru.t1.bugakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.enumerated.Role;
import ru.t1.bugakov.tm.model.User;

public interface IUserService extends IAbstractService<User> {

    @NotNull
    User create(@Nullable final String login, @Nullable final String password);

    @NotNull
    User create(@Nullable final String login, @Nullable final String password, @Nullable final String email);

    @NotNull
    User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role);

    @NotNull
    User findByLogin(@Nullable final String login);

    @NotNull
    User findByEmail(@Nullable final String email);

    @Nullable
    User removeByLogin(@Nullable final String login);

    @Nullable
    User removeByEmail(@Nullable final String email);

    @NotNull
    User setPassword(@Nullable final String id, @Nullable String password);

    @NotNull
    User updateUser(@Nullable final String id,
                    @Nullable final String firstName,
                    @Nullable final String lastName,
                    @Nullable final String middleName);

    boolean isLoginExist(@Nullable final String login);

    boolean isEmailExist(@Nullable final String email);

    @Nullable
    User lockUserByLogin(@Nullable final String login);

    @Nullable
    User unlockUserByLogin(@Nullable final String login);
}
