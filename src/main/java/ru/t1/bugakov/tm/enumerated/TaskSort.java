package ru.t1.bugakov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.comparator.CreatedComparator;
import ru.t1.bugakov.tm.comparator.NameComparator;
import ru.t1.bugakov.tm.comparator.StatusComparator;
import ru.t1.bugakov.tm.model.Task;

import java.util.Comparator;

@Getter
public enum TaskSort {

    BY_NAME("Sort by name", NameComparator.INSTANCE::compare),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE::compare),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE::compare);

    @NotNull
    private final String displayName;

    @NotNull
    private final Comparator<Task> comparator;

    TaskSort(@NotNull String displayName, @NotNull Comparator<Task> comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @Nullable
    public static TaskSort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (@NotNull final TaskSort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

}
